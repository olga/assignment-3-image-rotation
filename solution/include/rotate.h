#pragma once
#include "../include/image.h"
#include <inttypes.h>

void rotate(struct image *image, int64_t angle);
