#pragma once
#include "../include/bmp.h"
#include "../include/io_states.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

struct image
{
  uint64_t width, height;
  struct pixel *data;
};

enum read_status from_header_to_img(FILE *input, struct image *image, struct bmp_header *header);
void image_free(struct image *img);
