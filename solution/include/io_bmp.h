#pragma once
#include "../include/image.h"
#include "../include/io_states.h"
#include <stdio.h>
#include <stdlib.h>

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *image);
