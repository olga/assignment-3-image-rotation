#pragma once
#include <stdio.h>
#include <stdlib.h>

enum read_status
{
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_BIT_COUNT,
  READ_INVALID_PADDING
};

enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR,
  HEADER_ERROR
};
void close_file(FILE *file);
FILE *file_open(char *file_name, char *file_mode);
enum read_status has_read_error(FILE* file, enum read_status read_status);
enum write_status has_write_error(FILE* file, enum write_status write_status);
