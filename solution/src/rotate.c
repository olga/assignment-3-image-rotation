#include "../include/image.h"
#include "../include/bmp.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define ANGLE_BASE 90

 static void rotate_to_ange_base(struct image *image)
{
    struct pixel *pixel_buffer = malloc(sizeof(struct pixel) * image->width * image->height);

    uint32_t width = image->height;
    uint32_t height = image->width;

    int64_t width_for_iteration = (int64_t) width - 1;

    size_t index = 0;

    for (int64_t i = width_for_iteration; i >= 0; i--)
    {
        for (int64_t j = 0; j < height; j++)
        {
            size_t target_index = width * j + i;
            struct pixel pixel = image->data[index];
            pixel_buffer[target_index] = pixel;
            index++;
        }
    }

    free(image->data);
    image->data = pixel_buffer;
}

static void rotate_to_negative_ange_base(struct image *image)
{
    struct pixel *pixel_buffer = malloc(sizeof(struct pixel) * image->width * image->height);

    uint32_t width = image->height;
    uint32_t height = image->width;

    int64_t height_for_iteration = (int64_t) height - 1;

    size_t index = 0;

    for (int64_t i = 0; i < width; i++)
    {
        for (int64_t j = height_for_iteration; j >= 0; j--)
        {
            size_t target_index = width * j + i;
            struct pixel pixel = image->data[index];
            pixel_buffer[target_index] = pixel;
            index++;
        }
    }

    free(image->data);
    image->data = pixel_buffer;
}

void rotate(struct image *image, int32_t angle)
{
    if (angle % ANGLE_BASE == 0)
    {
        if (angle == 0)
        {
            return;
        }

        bool ifPositive = angle > 0;
        int8_t times = (int8_t) (abs(angle) / ANGLE_BASE);

        do
        {
            if (ifPositive)
            {
                rotate_to_ange_base(image);
            }
            else
            {
                rotate_to_negative_ange_base(image);
            }
            uint64_t buf_width = image->width;
            image->width = image->height;
            image->height = buf_width;

            times--;
        } while (times > 0);
    }
    else
    {
        fprintf(stderr, "Incorrect angle");
    }
}
