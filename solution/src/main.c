#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/io_bmp.h"
#include "../include/io_states.h"
#include "../include/rotate.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

static void end_program_err(char* message)
{
    fprintf(stderr, "%s \n", message);
    exit(1);
}

int main(int argc, char **argv)
{
    if(argc != 4)
    {
        end_program_err("Incorrect number of arguments");
    }

    char* file_name_input = argv[1];
    char* file_name_output = argv[2];
    int32_t angle = atoi(argv[3]);

    FILE *img_file = file_open(file_name_input, "rb");

    if(!img_file)
    {
        end_program_err("File opening fail");
    }

    struct image* image = malloc(sizeof(struct image));
    image->data = NULL;

    enum read_status io_states_read = from_bmp(img_file, image);

    if(!image->data)
    {
        end_program_err("Image read fail");
    }

    if(io_states_read != READ_OK)
    {
        end_program_err("Read fail");
    }

    rotate(image, angle);

    FILE *output_file = file_open(file_name_output, "wb");

    if(!output_file)
    {
        end_program_err("File opening fail");
    }

    enum write_status io_states_write = to_bmp(output_file, image);

    if(io_states_write != WRITE_OK)
    {
        end_program_err("Write fail");
    }


    image_free(image);

    return 0;
}
