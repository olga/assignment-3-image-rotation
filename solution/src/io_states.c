#include <stdio.h>

enum read_status
{
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_BIT_COUNT,
  READ_INVALID_PADDING
};

enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR,
  HEADER_ERROR
};

FILE *file_open(char *file_name, char *file_mode)
{
  FILE *file = fopen(file_name, file_mode);
  if (!file)
  {
    fprintf(stderr, "File opening error \n");
  }
  return file;
}

enum read_status has_read_error(FILE* file, enum read_status read_status)
{
    fclose(file);
  fprintf(stderr, "Read error \n");
  return read_status;
}

enum write_status has_write_error(FILE* file, enum write_status write_status)
{
    fclose(file);
  fprintf(stderr, "Write error \n");
  return write_status;
}
