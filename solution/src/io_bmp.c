#include "../include/io_states.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/io_bmp.h"
#include <inttypes.h>
#include <stdio.h>

#define BMP_TYPE_CODE 19778
#define BIT_COUNT 24
#define ONE_OBJ 1
#define MAX_PADDING_SIZE_ARRAY 3
#define PADDING_VALUE {0,0,0}

struct bmp_header g_main_header;

enum read_status from_bmp(FILE *input, struct image *img)
{
    struct bmp_header header = {0};

    if ((fread(&header, sizeof(struct bmp_header), ONE_OBJ, input)) != 1)
    {
        return has_read_error(input, READ_INVALID_HEADER);
    }
    if (header.bfType != BMP_TYPE_CODE)
    {
        return has_read_error(input, READ_INVALID_BITS);
    }
    if (header.biBitCount != BIT_COUNT)
    {
        return has_read_error(input, READ_INVALID_BIT_COUNT);
    }

    g_main_header = header;

    return from_header_to_img(input, img, &header);
}

enum write_status to_bmp(FILE *out, struct image const *image)
{
    struct bmp_header header = g_main_header;

    header.biWidth = image->width;
    header.biHeight = image->height;

    if (!fwrite(&header, sizeof(struct bmp_header), ONE_OBJ, out))
    {
        return has_write_error(out, HEADER_ERROR);
    }

    uint8_t padding_array[MAX_PADDING_SIZE_ARRAY] = PADDING_VALUE;

    int64_t height_for_iteration = (int64_t) image->height - 1;

    for (int64_t i = height_for_iteration; i >= 0; i--)
    {
        for (int64_t j = 0; j < image->width; j++)
        {
            struct pixel *one_pixel = image->data + image->width * i + j;

            if (!fwrite(one_pixel, sizeof(struct pixel), ONE_OBJ, out))
            {
                return has_write_error(out, WRITE_ERROR);
            }
        }
        fwrite(padding_array, image->width % 4, ONE_OBJ, out);
    }

    fclose(out);
    return WRITE_OK;
}
