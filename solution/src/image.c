#include "../include/io_states.h"
#include "../include/bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_TYPE_CODE 19778
#define BIT_COUNT 24
#define ONE_OBJ 1
#define MAX_PADDING 3

struct image
{
  uint64_t width, height;
  struct pixel *data;
};

enum read_status from_header_to_img(FILE *input, struct image *image, struct bmp_header *header)
{
  image->data = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);

  if (image->data)
  {
    image->width = header->biWidth;
    image->height = header->biHeight;

    uint8_t padding = (uint8_t)((header->biWidth) % 4);

    fseek(input, header->bOffBits, SEEK_SET);

    int64_t height_for_iteration = (int64_t) image->height - 1;

    for (int64_t i = height_for_iteration; i >= 0; i--)
    {
      for (int64_t j = 0; j < image->width; j++)
      {
        struct pixel *one_pixel = image->data + image->width * i + j;

        if(!(fread(one_pixel, sizeof(struct pixel), ONE_OBJ, input)))
        {
            return has_read_error(input, READ_ERROR);
        }
      }
      if (fseek(input, padding, SEEK_CUR) != 0)
      {
        return has_read_error(input, READ_INVALID_PADDING);
      }
    }

    fclose(input);
    return READ_OK;
  }
  else
  {
    return has_read_error(input, READ_INVALID_SIGNATURE);
  }
}

void image_free(struct image *image)
{
  if (image->data)
  {
    free(image->data);
    image->data = NULL;
  }
  free(image);
}
